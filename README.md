Dear candidate,

This is an exercise designed to test your ability to understand, structure and reason about code. In this repository, you will find a file with the source code taken from a DGL tutorial. We ask that you look through the code, try to understand it, and take notes on how the code can be improved. At the next interview, you get the opportunity to explain your thoughts in detail. In general, we are keen to hear your thoughts both on what the code does and how the code quality can be improved concerning extendability, maintainability, readability, efficiency, testability (this is a non-exhausted list, feel free to add any point that you think will improve the code quality).

In this repository you will find three files:
1. A file with source code, named main.py
2. A file with requirements, named requirements.txt
3. A virtual environment, named .venv (~800 MB)

It is recommended that you use the virtual environment to run the source code file as all dependencies are pre-installed in it. You can activate the virtual environment by typing the following into your terminal:

```source .venv/bin/activate```

If you need more assistance with virtual environments, please see the [Pipenv & Virtual Environments](https://docs.python-guide.org/dev/virtualenvs/) section of Kenneth Reitz's _The Hitchhiker's Guide to Python_. The requirements.txt file is there if you wish to download dependencies from pip and to provide you with an overview of the dependencies.

After you have activated the virtual environment you can run the code by typing the following into the terminal:

```python main.py```

If you experience any problems, feel free to reach out. We look forward to hearing your thoughts.