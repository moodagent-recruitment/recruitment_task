certifi==2021.5.30
chardet==4.0.0
decorator==4.4.2
dgl==0.6.1
idna==2.10
joblib==1.0.1
networkx==2.5.1
numpy==1.21.0
requests==2.25.1
scikit-learn==0.24.2
scipy==1.7.0
sklearn==0.0
threadpoolctl==2.1.0
torch==1.9.0
typing-extensions==3.10.0.0
urllib3==1.26.6